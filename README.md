### What is this repository for? ###

Repository for the code of the RoutesMobilityModel of ns-3.

The module allows the programmer to define which external service to contact to retrieve travel planning information, and makes ns-3 mobility traces out of it, to be used for example to define the routes taken by vehicles in simulations of vehicular communication.

Current implementation makes use of Google Maps services.

T. Cerqueira, M. Albano, “RoutesMobilityModel: easy realistic mobility simulation using external information services”, Workshop on ns-3 (WNS3 ‘15), May 13th, 2015, pp. 40-46, Castelldefels, Spain



### How do I get set up? ###

For the moment, you download the whole ns-3 framework put into this repository.

A few libraries are required beforehand:

 - [GeographicLib](http://geographiclib.sourceforge.net/)
 
 - [Xerces-C++](https://xerces.apache.org/xerces-c/)
 
 - [curlpp](http://www.curl4pp.org/)

Most of these libraries can be installed from your distribution's repositories. If you have any issues, please contact us and we'll do our best to help you get set up

The code is currently under review by ns-3 consortium, to be included into the ns-3 main branch.

### Who do I talk to? ###

Author: Tiago Cerqueira

Team members: Michele Albano

### Project Wiki ###
The wiki for the project can be found [here](https://www.nsnam.org/wiki/RoutesMobilityModel).

### WNS3 2015 presentation ###

The slides of the WNS3 presentation can be found [here](https://dl.dropboxusercontent.com/u/11539899/routesmobilitymodel_wns3_V1.pdf)